[![Typing SVG](https://readme-typing-svg.demolab.com?font=Poppins&weight=600&size=30&duration=5003&pause=1000&color=0077ff&width=436&height=44&lines=Hello+World!)](https://git.io/typing-svg#gh-light-mode-only)


I'm _Lukasz_ and I live in _Poland_.  
I'm interested in **Java** and  creating **web applications** in **Spring Boot**.

:trophy: **STATS**
---

![lukodziej wakatime stats](https://github-readme-stats.vercel.app/api/wakatime?username=lukodziej&custom_title=Most%20used%20languages%20in%20last%20365%20days&langs_count=6&layout=compact&hide=Other,GitIgnore%20file,Properties&theme=transparent&title_color=0077ff)

[*📊 Coding activity over Last 7 days (powered by WakaTime - gist plugin)*](https://gist.githubusercontent.com/lukkolo/c89c418acb7b7f9367935fee8c922911/raw)

<img src="https://leetcard.jacoblin.cool/light_lifter?theme=nord&font=Fira%20Sans&ext=activity" width="450" height="450">

💭 **ABOUT ME**
---
- 💻 **IT Admin**
- :bicyclist: **Road bike cyclist**
- :soccer:  **Football player**
- 🏎️ **Car enthusiast**

## 🔥 **SKILLS**

#### ⌨ **LANGUAGES, FRAMEWORKS & LIBRARIES**

[![My Skills](https://skillicons.dev/icons?i=java,spring,maven,docker,hibernate,postgres,mongodb,mysql,bash,git,jenkins,linux,postman,vim,)](https://skillicons.dev)


#### 🖥️ IDEs/Editors
![Ide](https://skillicons.dev/icons?i=idea,eclipse,vscode)
